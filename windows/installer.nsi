;Written by Joao Menezes
;--------------------------

!define TEMP1 $R0 ;Temp variable
!define UPDATE $R1 ;Update status variable
!define NEW_INSTANCE $R2 ;Second Instance of Connector variable
!define ERROR_MESSAGE $R3 ;Error message variable
RequestExecutionLevel user

;--------------------------------
;Include Modern UI

  !include "MUI2.nsh"

;--------------------------------

; String Replace Function
!include "lib\StrRep.nsi"

; String Contains Function
!include "lib\StrContains.nsi"

; Logic operations lib
!include LogicLib.nsh

;The name of the installer
Name "Agente Connector"

;The file to write
OutFile "Ipes-Connector-Setup.exe"

; Show install details
ShowInstDetails show

; Things that need to be extracted on startup (keep these lines before any File command!)
ReserveFile "installCustoms\connector.ini"
ReserveFile "installCustoms\connector2.ini"
ReserveFile "installCustoms\connector3.ini"

;Default installation text and folder
DirText "O instalador vai instalar o Agente Connector na pasta a seguir. Para selecionar um local diferente, clique em 'Selecionar' e escolha o novo destino. Clique em 'Install' para instalar o Connector." "Pasta do Agente Connector" "Selecionar"
InstallDir "C:\Ipes-Connector\Connector"


; Welcome page title and text
!define MUI_WELCOMEPAGE_TITLE "Instalador do Agente Connector"
!define MUI_WELCOMEPAGE_TEXT "Bem vindo ao instalador do Agente Connector. Clique em 'Next' para continuar."

; Welcome page title and text
!define MUI_INSTFILES_TITLE "Instalando o Agente Connector"
!define MUI_INSTFILES_TEXT "Instalando o Agente Connector com todos os seus componentes..."

; Order of pages
!insertmacro MUI_PAGE_WELCOME
Page custom SetCustom ValidateCustom "";Custom page. InstallOptions gets called in SetCustom.
Page custom SetCustom2 ValidateCustom2 "" ;Custom page. InstallOptions2 gets called in SetCustom.
Page custom SetCustom3 ValidateCustom3 "" ;Custom page. InstallOptions2 gets called in SetCustom.
!define MUI_PAGE_CUSTOMFUNCTION_LEAVE validateDirectory
!insertmacro MUI_PAGE_DIRECTORY
; !insertmacro MUI_PAGE_INSTFILES
Page instfiles
;Interface Settings

  !define MUI_ABORTWARNING

;--------------------------------
; Languages
 
  !insertmacro MUI_LANGUAGE "English"

; --------------------------------
; Section "Components"
  
; SectionEnd

Section "Dummy Section" SecDummy

  Var /GLOBAL javaPath

  SetOutPath "$INSTDIR\bin"
  File /nonfatal /a /r "bin\"
  
  SetOutPath "$INSTDIR"
  
  File connector.jar
  File connector.properties
  CreateDirectory "$INSTDIR\scripts"
  Call writePIDGetter
  Call writeUpdaterChecker


  FileOpen $4 "$INSTDIR\java_path.txt" w
  FileWrite $4 '$javaPath'
  FileClose $4


  FileOpen $9 "$INSTDIR\connector.properties" a
  FileSeek $9 0 END
  ;Get Install Options dialog user input

  FileWrite $9 "#CNES do estabelecimento de saude executando o agente$\r$\n"
  ReadINIStr ${TEMP1} "$PLUGINSDIR\connector.ini" "Field 3" "State"
  DetailPrint "agent.organization.cnes=${TEMP1}"
  FileWrite $9 "agent.organization.cnes=${TEMP1}$\r$\n"

  FileWrite $9 "#Responsavel pela execucao do agente no estabelecimento de saude$\r$\n"
  ReadINIStr ${TEMP1} "$PLUGINSDIR\connector.ini" "Field 5" "State"
  DetailPrint "agent.organization.contactPerson=${TEMP1}"
  FileWrite $9 "agent.organization.contactPerson=${TEMP1}$\r$\n"

  FileWrite $9 "#Intervalo de tempo para considerar na extracao de RAC$\r$\n"
  ReadINIStr ${TEMP1} "$PLUGINSDIR\connector.ini" "Field 7" "State"
  DetailPrint "extractor.sql.minDate=${TEMP1}"
  FileWrite $9 "extractor.sql.minDate=${TEMP1}$\r$\n"

  ReadINIStr ${TEMP1} "$PLUGINSDIR\connector.ini" "Field 9" "State"
  DetailPrint "extractor.sql.maxDate=${TEMP1}"
  FileWrite $9 "extractor.sql.maxDate=${TEMP1}$\r$\n"

  FileWrite $9 "#Token para consumo dos servicos na plataforma$\r$\n"
  ReadINIStr ${TEMP1} "$PLUGINSDIR\connector.ini" "Field 11" "State"
  DetailPrint "agent.credentials.token=${TEMP1}"
  FileWrite $9 "agent.credentials.token=${TEMP1}$\r$\n"

  FileWrite $9 "#Numero sequencial identificador da instancia do connector em uma maquina$\r$\n"
  ReadINIStr ${TEMP1} "$PLUGINSDIR\connector3.ini" "Field 5" "State"
  DetailPrint "agent.custodian.id.sequencial=${TEMP1}"
  FileWrite $9 "agent.custodian.id.sequencial=${TEMP1}$\r$\n"

  FileWrite $9 "#Parametros de conexao com o banco de dados$\r$\n"
  ReadINIStr ${TEMP1} "$PLUGINSDIR\connector2.ini" "Field 3" "State"
  DetailPrint "jdbc.url=${TEMP1}"
  FileWrite $9 "jdbc.url=${TEMP1}$\r$\n"

  ReadINIStr ${TEMP1} "$PLUGINSDIR\connector2.ini" "Field 5" "State"
  DetailPrint "jdbc.user=${TEMP1}"
  FileWrite $9 "jdbc.user=${TEMP1}$\r$\n"

  ReadINIStr ${TEMP1} "$PLUGINSDIR\connector2.ini" "Field 7" "State"
  DetailPrint "jdbc.password=${TEMP1}"
  FileWrite $9 "jdbc.password=${TEMP1}$\r$\n"
  
  ReadINIStr ${TEMP1} "$PLUGINSDIR\connector2.ini" "Field 9" "State"
  DetailPrint "scheduler.cron=${TEMP1}"
  FileWrite $9 "scheduler.cron=${TEMP1}$\r$\n"

  
  ReadINIStr ${TEMP1} "$PLUGINSDIR\connector2.ini" "Field 11" "State"
  FileWrite $9 "#Arquivo de carregamento de beans do Spring para provisionamento dinamico de modulos$\r$\n"
  ${If} ${TEMP1} == "PEC"
    FileWrite $9 "spring.beans.location=classpath:/beans_PEP.xml$\r$\n"
    DetailPrint "spring.beans.location=classpath:/beans_PEP.xml"
  ${Else}
    ${If} ${TEMP1} == "Centralizador"
      FileWrite $9 "spring.beans.location=classpath:/beans_CDS.xml$\r$\n"
      DetailPrint "spring.beans.location=classpath:/beans_CDS.xml"
    ${Else}
      FileWrite $9 "spring.beans.location=classpath:/beans_GSEA.xml$\r$\n"
      DetailPrint "spring.beans.location=classpath:/beans_GSEA.xml"
    ${EndIf}
  ${EndIf}
  FileClose $9

  Exec "copy $\"$INSTDIR\connector.properties$\" application.properties"
  
  Call WriteScheduledEvent

  Call WriteExecutor
  


  FileOpen $4 "$INSTDIR\ipes-connector-initializer.bat" w
  FileWrite $4 'cd $INSTDIR $\r$\n'
  FileWrite $4 '"$INSTDIR\ipes-connector.vbs"'
  FileClose $4

  FileOpen $4 "$INSTDIR\connector_stopper.bat" w
  FileWrite $4 'set /p var=<"$INSTDIR\PID.txt"$\r$\n'
  FileWrite $4 'taskkill -pid %var% -f$\r$\n'
  FileClose $4

  FileOpen $4 "$INSTDIR\ipes-connector.vbs" w
  
  FileWrite $4 'Set oShell = CreateObject ("Wscript.Shell") $\r$\n'
  FileWrite $4 'Dim strArgs$\r$\n'
  FileWrite $4 'strArgs = "cmd /c ""$INSTDIR\executor.bat"""$\r$\n'
  FileWrite $4 'oShell.Run strArgs, 0, false$\r$\n'

  FileClose $4

  ReadINIStr ${TEMP1} "$PLUGINSDIR\connector3.ini" "Field 5" "State"
  DetailPrint "agent.custodian.id.sequencial=${TEMP1}"
  CreateShortCut "$SMPROGRAMS\Startup\ipes-connector-${TEMP1}.lnk" "$INSTDIR\ipes-connector.vbs"
  
  ; Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"

  ReadINIStr ${TEMP1} "$PLUGINSDIR\connector3.ini" "Field 5" "State"
  ;Create scheduled event
  ExecWait 'schtasks.exe /Create /XML "$INSTDIR\ipes-connector-updater.xml" /tn ipes-connector-${TEMP1}-updater'

  ; Initialize connector
  Exec '"$INSTDIR\ipes-connector-initializer.bat"'

  Delete "$INSTDIR\ipes-connector-updater.xml"

SectionEnd

;Uninstaller Section
Section "Uninstall"

  ;ADD YOUR OWN FILES HERE...
  Delete "$SMPROGRAMS\Startup\ipes-connector.lnk"
  Delete "$SMPROGRAMS\Startup\ipes-connector"

  FileOpen $4 "$INSTDIR\PID.txt" r
  getPIDUninstallLoop:
  FileRead $4 $1
  StrCmp $1 "$\n" getPIDUninstallLoop
  StrCmp $1 "$\n$\r" getPIDUninstallLoop
  StrCmp $1 "$\r$\n" getPIDUninstallLoop
  ${StrRep} $1 $1 "$\r" ""
  ${StrRep} $1 $1 "$\n" ""
  ${StrRep} $1 $1 " " ""
  FileClose $4

  ; MessageBox MB_OK "taskkill -pid $1 -f"
  ; nsExec::Exec  "taskkill -pid $1 -f"
  ExecWait  "taskkill -pid $1 -f"

  Delete "$INSTDIR\connector.properties"
  Delete "$INSTDIR\connector.jar"
  Delete "$INSTDIR\executor.bat"
  Delete "$INSTDIR\PatientData.timestamp"
  Delete "$INSTDIR\RACData.timestamp"
  Delete "$INSTDIR\ipes-connector-initializer.bat"
  
  RMDir /r "$INSTDIR\xml"
  RMDir /r "$INSTDIR\csv"
  RMDir /r "$INSTDIR\h2"
  RMDir /r "$INSTDIR\log"

  Delete "$INSTDIR\Uninstall.exe"
  RMDir /r "$INSTDIR"

SectionEnd

Function .onInit

  ;Extract InstallOptions files
  ;$PLUGINSDIR will automatically be removed when the installer closes

  InitPluginsDir
  File /oname=$PLUGINSDIR\connector.ini "installCustoms\connector.ini"
  File /oname=$PLUGINSDIR\connector2.ini "installCustoms\connector2.ini"
  File /oname=$PLUGINSDIR\connector3.ini "installCustoms\connector3.ini"

  
  Var /GLOBAL line
  Var /GLOBAL comment
  Var /GLOBAL property
  Var /GLOBAL value


FunctionEnd

Function SetCustom

  ; ${If} ${UPDATE} == false
  ;   Goto normalInstalation
  ; ${Else}
  ;   Call FillCustom
  ; ${EndIf}
  ; IfFileExists $SMPROGRAMS\Startup\ipes-connector.lnk 0 +2
  Call FillCustom

    Push ${TEMP1}

      InstallOptions::dialog "$PLUGINSDIR\connector.ini"
      Pop ${TEMP1}

    Pop ${TEMP1}

FunctionEnd

Function SetCustom2

  ;Display the InstallOptions dialog

  Push ${TEMP1}

    InstallOptions::dialog "$PLUGINSDIR\connector2.ini"
    Pop ${TEMP1}

  Pop ${TEMP1}

FunctionEnd

Function SetCustom3

  ;Display the InstallOptions dialog

  Push ${TEMP1}

    InstallOptions::dialog "$PLUGINSDIR\connector3.ini"
    Pop ${TEMP1}

  Pop ${TEMP1}

FunctionEnd

Function ValidateCustom

  ReadINIStr ${TEMP1} "$PLUGINSDIR\connector.ini" "Field 3" "State"
  StrCmp ${TEMP1} "" error

  ReadINIStr ${TEMP1} "$PLUGINSDIR\connector.ini" "Field 5" "State"
  StrCmp ${TEMP1} "" error

  ReadINIStr ${TEMP1} "$PLUGINSDIR\connector.ini" "Field 7" "State"
  StrCmp ${TEMP1} "" error

  ReadINIStr ${TEMP1} "$PLUGINSDIR\connector.ini" "Field 9" "State"
  StrCmp ${TEMP1} "" error


  Goto done

  error:
    MessageBox MB_ICONEXCLAMATION|MB_OK "Preencha todos os campos!"
    Abort
  done:

FunctionEnd

Function ValidateCustom2

  ReadINIStr ${TEMP1} "$PLUGINSDIR\connector2.ini" "Field 3" "State"
  StrCmp ${TEMP1} "" error
  
  ReadINIStr ${TEMP1} "$PLUGINSDIR\connector2.ini" "Field 5" "State"
  StrCmp ${TEMP1} "" error

  ReadINIStr ${TEMP1} "$PLUGINSDIR\connector2.ini" "Field 7" "State"
  StrCmp ${TEMP1} "" error

  ReadINIStr ${TEMP1} "$PLUGINSDIR\connector2.ini" "Field 9" "State"
  StrCmp ${TEMP1} "" error

  ReadINIStr ${TEMP1} "$PLUGINSDIR\connector2.ini" "Field 11" "State" 
  StrCmp ${TEMP1} "" error

  Goto done

  javaPathError:
    MessageBox MB_ICONEXCLAMATION|MB_OK "Indique o caminho para o executavel do java 8!"
    Abort

  error:
    MessageBox MB_ICONEXCLAMATION|MB_OK "Preencha todos os campos!"
    Abort
  done:

FunctionEnd

Function ValidateCustom3

  ReadINIStr ${TEMP1} "$PLUGINSDIR\connector3.ini" "Field 3" "State"
  ${StrContains} $property ".exe" ${TEMP1}
  StrCmp $property "" javaPathError
  StrCpy $javaPath ${TEMP1}
  ${StrRep} $javaPath $javaPath "java.exe" ""

  ReadINIStr ${TEMP1} "$PLUGINSDIR\connector3.ini" "Field 5" "State"
  StrCmp ${TEMP1} "" error
  StrCpy $INSTDIR "$INSTDIR-${TEMP1}"

  Goto done

  javaPathError:
    MessageBox MB_ICONEXCLAMATION|MB_OK "Indique o caminho para o executavel do java 8!"
    Abort

  error:
    MessageBox MB_ICONEXCLAMATION|MB_OK "Preencha todos os campos!"
    Abort
  done:
  
FunctionEnd

Function FillCustom
  Push ${TEMP1}
  FileOpen $4 "application.properties" r
    Goto LOOP
      LOOP:
        FileRead $4 $line
        IfErrors exit_loop
        ${StrContains} $comment "#" $line

        StrCmp $comment "" continue
        Goto LOOP
        continue:
        ; MessageBox MB_OK $line

        checkCNES:
        ${StrContains} $property "agent.organization.cnes" $line
        StrCmp $property "" checkNome
        ${StrRep} $value $line "agent.organization.cnes=" ""
        WriteINIStr "$PLUGINSDIR\connector.ini" "Field 3" "State" $value
        Goto LOOP

        checkNome:
        ${StrContains} $property "agent.organization.contactPerson" $line
        StrCmp $property "" checkMinDate
        ${StrRep} $value $line "agent.organization.contactPerson=" ""
        WriteINIStr "$PLUGINSDIR\connector.ini" "Field 5" "State" $value
        Goto LOOP

        checkMinDate:
        ${StrContains} $property "extractor.sql.minDate" $line
        StrCmp $property "" checkMaxDate
        ${StrRep} $value $line "extractor.sql.minDate=" ""
        WriteINIStr "$PLUGINSDIR\connector.ini" "Field 7" "State" $value
        Goto LOOP

        checkMaxDate:
        ${StrContains} $property "extractor.sql.maxDate" $line
        StrCmp $property "" checkToken
        ${StrRep} $value $line "extractor.sql.maxDate=" ""
        WriteINIStr "$PLUGINSDIR\connector.ini" "Field 9" "State" $value
        Goto LOOP

        checkToken:
        ${StrContains} $property "agent.credentials.token" $line
        StrCmp $property "" checkJDBC
        ${StrRep} $value $line "agent.credentials.token=" ""
        WriteINIStr "$PLUGINSDIR\connector.ini" "Field 11" "State" $value
        Goto LOOP

        checkJDBC:
        ${StrContains} $property "jdbc.url" $line
        StrCmp $property "" checkUsername
        ${StrRep} $value $line "jdbc.url=" ""
        WriteINIStr "$PLUGINSDIR\connector2.ini" "Field 3" "State" $value
        Goto LOOP

        checkUsername:
        ${StrContains} $property "jdbc.user" $line
        StrCmp $property "" checkPassword
        ${StrRep} $value $line "jdbc.user=" ""
        WriteINIStr "$PLUGINSDIR\connector2.ini" "Field 5" "State" $value
        Goto LOOP

        checkPassword:
        ${StrContains} $property "jdbc.password" $line
        StrCmp $property "" checkSequential
        ${StrRep} $value $line "jdbc.password=" ""
        WriteINIStr "$PLUGINSDIR\connector2.ini" "Field 7" "State" $value
        Goto LOOP

        checkSequential:
        ${StrContains} $property "agent.custodian.id.sequencial" $line
        StrCmp $property "" LOOP
        ${StrRep} $value $line "agent.custodian.id.sequencial=" ""
        WriteINIStr "$PLUGINSDIR\connector3.ini" "Field 5" "State" $value
        Goto LOOP

      exit_loop:
  FileClose $4
  Pop ${TEMP1}
FunctionEnd

Function writePIDGetter
  FileOpen $4 "$INSTDIR\scripts\pidGetter.sh" w
  FileWrite $4 '#!/bin/bash $\r$\n'
  FileWrite $4 ' $\r$\n'
  FileWrite $4 'cd "$INSTDIR"$\r$\n'
  FileWrite $4 ' $\r$\n'
  FileWrite $4 'properties_path="$INSTDIR"$\r$\n'
  FileWrite $4 `properties_path=$$(echo "$${properties_path}" | sed 's/\\/\\\\/g')$\r$\n`
  FileWrite $4 ' $\r$\n'
  FileWrite $4 'jps_output=$$(cmd.exe cmd /c "$javaPath\jps" -v -m) $\r$\n'
  FileWrite $4 'connector_pid=$$(echo "$${jps_output}" | sed "s/\([0-9]\+\) \(.*\)\($${properties_path}\)\(.*\)/\1/")$\r$\n'
  FileWrite $4 `connector_pid=$$(echo "$${connector_pid}" | sed 's/[0-9]\+ .*//')$\r$\n`
  FileWrite $4 ' $\r$\n'
  FileWrite $4 'echo "$${connector_pid}" > "$INSTDIR/TEMP_PID.txt"$\r$\n'
  FileWrite $4 'awk NF "$INSTDIR/TEMP_PID.txt" > "$INSTDIR/PID.txt"$\r$\n'
  FileWrite $4 'rm "$INSTDIR/TEMP_PID.txt"$\r$\n'
  FileClose $4
FunctionEnd

Function stopConnector
  ClearErrors
  Var /GLOBAL firstRun
  StrCpy $firstRun true
  Call writePIDGetter

  getPID:
  FileOpen $4 "${TEMP1}\PID.txt" r
  getPIDLoop:
  FileRead $4 $1
  StrCmp $1 "$\n" getPIDLoop
  StrCmp $1 "$\n$\r" getPIDLoop
  StrCmp $1 "$\r$\n" getPIDLoop
  IfErrors noPID
  ${StrRep} $1 $1 "$\r" ""
  ${StrRep} $1 $1 "$\n" ""
  ${StrRep} $1 $1 " " ""
  FileClose $4

  StrCmp $1 "$\n" noConnectorRunning
  ClearErrors
  ; MessageBox MB_OK  "taskkill -pid $1 -f"
  ; ExecWait  "taskkill -pid $1 -f"
  ExecWait  "taskkill -pid $1 -f"
  IfErrors errorStoppingPID
  Goto noConnectorRunning

  noPID:
  ExecWait '"$INSTDIR\bin\bash.exe" "$INSTDIR\scripts\pidGetter.sh"'
  StrCmp $firstRun false errorGettingPID
  StrCpy $firstRun false
  Goto getPID

  errorGettingPID:
  MessageBox MB_OK  "Nao foi possivel identificar o PID do agente em execucao.$\nCertifique-se de que ele esta parado para continuar com a atualizacao"
  Goto noConnectorRunning
  errorStoppingPID:
  MessageBox MB_OK  "Nao foi possivel interromper o agente em execucao.$\nCertifique-se de que ele esta parado para continuar com a atualizacao"


  noConnectorRunning:
FunctionEnd

Function WriteExecutor
  Push ${TEMP1}
  ReadINIStr ${TEMP1} "$PLUGINSDIR\connector3.ini" "Field 3" "State"
  FileOpen $4 "$INSTDIR\executor.bat" w
  FileWrite $4 'cd $INSTDIR$\r$\n'
  FileWrite $4 'start /b cmd /c ""${TEMP1}" -Dfile.encoding=UTF-8 -Dsun.stdout.encoding=UTF-8 -Dsun.err.encoding=UTF-8 -Xmx2g -XX:+UseG1GC -XX:MinHeapFreeRatio=10 -XX:MaxHeapFreeRatio=30 -XX:+UseStringDeduplication -jar "$INSTDIR\connector.jar"  --spring.config.location="$INSTDIR\connector.properties""$\r$\n'
  FileWrite $4 'bin\bash.exe scripts\pidGetter.sh$\r$\n'
  FileClose $4
  Pop ${Temp1}
FunctionEnd

Function validateDirectory

  
  ClearErrors
  CreateDirectory "$INSTDIR"
  FileOpen $R0 "$INSTDIR\temp_verifier.txt" w
  FileClose $R0
  IfErrors 0 validateDirectoryEnd
  MessageBox MB_OK "O local indicado e' invalido por necessitar de permissoes de administrador.$\nEscolha outro local para instalacao."
  Abort

  validateDirectoryEnd:
  
  Delete "$INSTDIR\temp_verifier.txt"
FunctionEnd

Function writeUpdaterChecker
  FileOpen $4 "$INSTDIR\scripts\updater_checker.sh" w
  FileWrite $4 `#!/bin/bash$\r$\n`
  FileWrite $4 `cd "$INSTDIR"$\r$\n`
  FileWrite $4 `version_link=https://bitbucket.org/coreconsultoria/res-connector-agent-core-installers/raw/master/version.json$\r$\n`
  FileWrite $4 `updater_link=https://bitbucket.org/coreconsultoria/res-connector-agent-core-installers/raw/master/windows%20updater/updater_runner.sh$\r$\n`
  FileWrite $4 `$\r$\n`
  FileWrite $4 `update() {$\r$\n`
  FileWrite $4 `    curl $$updater_link --output updater_runner.sh -s$\r$\n`
  FileWrite $4 `    bin/bash.exe ./updater_runner.sh > updater_log.txt$\r$\n`
  FileWrite $4 `    rm updater_runner.sh$\r$\n`
  FileWrite $4 `}$\r$\n`
  FileWrite $4 `$\r$\n`
  FileWrite $4 `"$javaPath\jar" xf connector.jar META-INF/MANIFEST.MF$\r$\n`
  FileWrite $4 `$\r$\n`
  FileWrite $4 `local_version=$$(sed -nr '/Implementation-Version: /p' META-INF/MANIFEST.MF)$\r$\n`
  FileWrite $4 `local_version=$$(echo $${local_version} | sed 's/\(Implementation-Version: \)\(.*\)/\2/') $\r$\n`
  FileWrite $4 `$\r$\n`
  FileWrite $4 `current_version=$$(curl $$version_link -s)$\r$\n`
  FileWrite $4 `current_version=$$(echo "$$current_version" | sed -r 's/[^0-9.]//g')$\r$\n`
  FileWrite $4 `current_version=$$(echo "$$current_version" | sed -r 's/\s//g')$\r$\n`
  FileWrite $4 `$\r$\n`
  FileWrite $4 `if [ "$$current_version" != "$$local_version" ]; then$\r$\n`
  FileWrite $4 `    update$\r$\n`
  FileWrite $4 `fi$\r$\n`
  FileWrite $4 `echo fim$\r$\n`
  FileWrite $4 `rm -r META-INF$\r$\n`
  FileClose $4
FunctionEnd

Function WriteScheduledEvent
  FileOpen $9 "$INSTDIR\ipes-connector-updater.xml" w
  FileWrite $9 '<?xml version="1.0" encoding="UTF-16"?>$\r$\n'
  FileWrite $9 '<Task version="1.4" xmlns="http://schemas.microsoft.com/windows/2004/02/mit/task">$\r$\n'
  FileWrite $9 '  <RegistrationInfo>$\r$\n'
  FileWrite $9 '    <Date>2020-08-06T11:50:42.2789596</Date>$\r$\n'
  FileWrite $9 '    <Author>Agente Conector</Author>$\r$\n'
  FileWrite $9 '    <URI>\ipes-connector-updater</URI>$\r$\n'
  FileWrite $9 '  </RegistrationInfo>$\r$\n'
  FileWrite $9 '  <Triggers>$\r$\n'
  FileWrite $9 '    <CalendarTrigger>$\r$\n'
  FileWrite $9 '      <StartBoundary>2020-08-06T12:30:00</StartBoundary>$\r$\n'
  FileWrite $9 '      <Enabled>true</Enabled>$\r$\n'
  FileWrite $9 '      <ScheduleByDay>$\r$\n'
  FileWrite $9 '        <DaysInterval>1</DaysInterval>$\r$\n'
  FileWrite $9 '      </ScheduleByDay>$\r$\n'
  FileWrite $9 '    </CalendarTrigger>$\r$\n'
  FileWrite $9 '  </Triggers>$\r$\n'
  FileWrite $9 '  <Settings>$\r$\n'
  FileWrite $9 '    <MultipleInstancesPolicy>StopExisting</MultipleInstancesPolicy>$\r$\n'
  FileWrite $9 '    <DisallowStartIfOnBatteries>false</DisallowStartIfOnBatteries>$\r$\n'
  FileWrite $9 '    <StopIfGoingOnBatteries>false</StopIfGoingOnBatteries>$\r$\n'
  FileWrite $9 '    <AllowHardTerminate>false</AllowHardTerminate>$\r$\n'
  FileWrite $9 '    <StartWhenAvailable>true</StartWhenAvailable>$\r$\n'
  FileWrite $9 '    <RunOnlyIfNetworkAvailable>false</RunOnlyIfNetworkAvailable>$\r$\n'
  FileWrite $9 '    <IdleSettings>$\r$\n'
  FileWrite $9 '      <StopOnIdleEnd>true</StopOnIdleEnd>$\r$\n'
  FileWrite $9 '      <RestartOnIdle>false</RestartOnIdle>$\r$\n'
  FileWrite $9 '    </IdleSettings>$\r$\n'
  FileWrite $9 '    <AllowStartOnDemand>true</AllowStartOnDemand>$\r$\n'
  FileWrite $9 '    <Enabled>true</Enabled>$\r$\n'
  FileWrite $9 '    <Hidden>false</Hidden>$\r$\n'
  FileWrite $9 '    <RunOnlyIfIdle>false</RunOnlyIfIdle>$\r$\n'
  FileWrite $9 '    <DisallowStartOnRemoteAppSession>false</DisallowStartOnRemoteAppSession>$\r$\n'
  FileWrite $9 '    <UseUnifiedSchedulingEngine>true</UseUnifiedSchedulingEngine>$\r$\n'
  FileWrite $9 '    <WakeToRun>false</WakeToRun>$\r$\n'
  FileWrite $9 '    <ExecutionTimeLimit>PT1H</ExecutionTimeLimit>$\r$\n'
  FileWrite $9 '    <Priority>7</Priority>$\r$\n'
  FileWrite $9 '  </Settings>$\r$\n'
  FileWrite $9 '<Actions Context="Author">$\r$\n'
  FileWrite $9 '  <Exec>$\r$\n'
  FileWrite $9 '    <Command>"$INSTDIR\bin\bash.exe"</Command>$\r$\n'
  FileWrite $9 '    <Arguments>"$INSTDIR\scripts\updater_checker.sh"</Arguments>$\r$\n'
  FileWrite $9 '  </Exec>$\r$\n'
  FileWrite $9 '</Actions>$\r$\n'
  FileWrite $9 '</Task>$\r$\n'
  FileClose $9
FunctionEnd
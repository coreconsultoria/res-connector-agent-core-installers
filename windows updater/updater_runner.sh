#!/bin/bash

echo Updating connector
echo Stopping connector...
cmd.exe  "/c connector_stopper.bat"
echo Connector Stopped

mv connector.jar old_connector.jar

echo Getting new connector package...
curl https://bitbucket.org/coreconsultoria/res-connector-agent-core-installers/raw/master/connector.jar --output connector.jar -s
echo new connector package downloaded

echo Executing updated connector agent...
cmd.exe  "/c ipes-connector-initializer.bat"
rm old_connector.jar
echo Updated connector agent running

echo successfully updated connector agent!